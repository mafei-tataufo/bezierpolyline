package com.example.bezierpolyline

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import kotlin.math.abs
import kotlin.math.sqrt

class BezierPolylineView2 @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val path = Path()

    private val paint = Paint().apply {
        color = Color.parseColor("#FFFF00")
        style = Paint.Style.STROKE
        strokeWidth = 10f
    }

    private var pathEffect: PathEffect = DashPathEffect(floatArrayOf(15f, 10f), 0f)

    /**
     * 相对于图片的坐标
     */
    private var control12 = PointF(400f - 66f, 510f)
    private var control23 = PointF(400f + 66f, 510f)

    private var control34 = PointF(800f - 66f, 510f)
    private var control45 = PointF(800f + 66f, 510f)

    private val point1 = PointF(200f, 510f)
    private val point2 = PointF(400f, 510f)
    private val point3 = PointF(600f, 510f)
    private val point4 = PointF(800f, 510f)
    private val point5 = PointF(1000f, 510f)


    private var imgRect = RectF()
    private var imgScale = 1f

    fun setImageRect(rect: RectF, scale: Float) {
        imgRect = rect
        imgScale = scale

        calculate()
        calculate1()
        invalidate()
    }

    var isShowHelpLine = false
        set(value) {
            field = value
            invalidate()
        }

    private fun getXOnView(xOnImg: Float): Float {
        return imgRect.left + xOnImg * imgScale
    }

    private fun getYOnView(yOnImg: Float): Float {
        return imgRect.top + yOnImg * imgScale
    }

    private fun getXOnImg(xOnView: Float): Float {
        return (xOnView - imgRect.left) / imgScale
    }

    private fun getYOnImg(yOnView: Float): Float {
        return (yOnView - imgRect.top) / imgScale
    }

    private var distance1 = 0f
    private var distance2 = 0f

    private var isBeeline = true

    override fun onDraw(canvas: Canvas) {
        if (isBeeline) {
            path.reset()
            path.moveTo(getXOnView(point1.x), getYOnView(point1.y))
            path.lineTo(getXOnView(point2.x), getYOnView(point2.y))
            path.lineTo(getXOnView(point3.x), getYOnView(point3.y))
            path.lineTo(getXOnView(point4.x), getYOnView(point4.y))
            path.lineTo(getXOnView(point5.x), getYOnView(point5.y))

            canvas.drawPath(path, paint)

            canvas.drawCircle(getXOnView(point1.x), getYOnView(point1.y), 8f, paint)
            canvas.drawCircle(getXOnView(point2.x), getYOnView(point2.y), 8f, paint)
            canvas.drawCircle(getXOnView(point3.x), getYOnView(point3.y), 8f, paint)
            canvas.drawCircle(getXOnView(point4.x), getYOnView(point4.y), 8f, paint)
            canvas.drawCircle(getXOnView(point5.x), getYOnView(point5.y), 8f, paint)

        } else {
            if (isShowHelpLine) {
                paint.pathEffect = pathEffect
                canvas.drawLine(getXOnView(control12.x), getYOnView(control12.y), getXOnView(control23.x), getYOnView(control23.y), paint)
                canvas.drawLine(getXOnView(control34.x), getYOnView(control34.y), getXOnView(control45.x), getYOnView(control45.y), paint)
                paint.pathEffect = null
            }

            // 第一段
            path.reset()
            path.moveTo(getXOnView(point1.x), getYOnView(point1.y))
            path.quadTo(getXOnView(control12.x), getYOnView(control12.y), getXOnView(point2.x), getYOnView(point2.y))

            canvas.drawPath(path, paint)

            // 第二段
            path.reset()
            path.moveTo(getXOnView(point2.x), getYOnView(point2.y))
            path.quadTo(getXOnView(control23.x), getYOnView(control23.y), getXOnView(point3.x), getYOnView(point3.y))

            canvas.drawPath(path, paint)

            // 第三段
            path.reset()
            path.moveTo(getXOnView(point3.x), getYOnView(point3.y))
            path.quadTo(getXOnView(control34.x), getYOnView(control34.y), getXOnView(point4.x), getYOnView(point4.y))

            canvas.drawPath(path, paint)

            // 第四段
            path.reset()
            path.moveTo(getXOnView(point4.x), getYOnView(point4.y))
            path.quadTo(getXOnView(control45.x), getYOnView(control45.y), getXOnView(point5.x), getYOnView(point5.y))

            canvas.drawPath(path, paint)


            canvas.drawCircle(getXOnView(point1.x), getYOnView(point1.y), 8f, paint)
            canvas.drawCircle(getXOnView(point2.x), getYOnView(point2.y), 8f, paint)
            canvas.drawCircle(getXOnView(point3.x), getYOnView(point3.y), 8f, paint)
            canvas.drawCircle(getXOnView(point4.x), getYOnView(point4.y), 8f, paint)
            canvas.drawCircle(getXOnView(point5.x), getYOnView(point5.y), 8f, paint)

            if (isShowHelpLine) {
                canvas.drawCircle(getXOnView(control12.x), getYOnView(control12.y), 8f, paint)
                canvas.drawCircle(getXOnView(control23.x), getYOnView(control23.y), 8f, paint)
                canvas.drawCircle(getXOnView(control34.x), getYOnView(control34.y), 8f, paint)
                canvas.drawCircle(getXOnView(control45.x), getYOnView(control45.y), 8f, paint)
            }
        }

        // 计算控制杆一半的距离
        // 两个端点距离的三分之一的一半
        val dx1 = abs(point1.x - point3.x)
        val dy1 = abs(point1.y - point3.y)
        distance1 = sqrt(dx1 * dx1 + dy1 * dy1) / 3 / 2

        val dx2 = abs(point3.x - point5.x)
        val dy2 = abs(point3.y - point5.y)
        distance2 = sqrt(dx2 * dx2 + dy2 * dy2) / 3 / 2
    }

    private fun calculate() {
        // 两个端点的直线斜率
        val kLine = (point3.y - point1.y) / (point3.x - point1.x)
        // 控制杆的直线 b
        val bLine = point2.y - kLine * point2.x

        // 圆方程的参数 ax2 + bx + c = 0
        val aCircle = 1 + (kLine * kLine)
        val bCircle = 2 * kLine * (bLine - point2.y) - (2 * point2.x)
        val cCircle = (point2.x * point2.x) + ((bLine - point2.y) * (bLine - point2.y)) - (distance1 * distance1)

        val x1 = (-bCircle + sqrt(bCircle * bCircle - (4 * aCircle * cCircle))) / (2 * aCircle)
        val x2 = (-bCircle - sqrt(bCircle * bCircle - (4 * aCircle * cCircle))) / (2 * aCircle)

        val y1 = kLine * x1 + bLine
        val y2 = kLine * x2 + bLine

        val result1 = PointF(x1, y1)
        val result2 = PointF(x2, y2)

        val distanceY1 = result1.y - result2.y

        Log.e("mafei", "distanceY1=" + distanceY1)
        Log.e("mafei", "distance1=" + distance1)

        if (abs(distanceY1) > distance1 * 2) {
            Log.e("mafei", "异常：太长")
            return
        }

        if (abs(distanceY1) < 1) {
            Log.e("mafei", "异常：< 1")
            return
        }

        if (distanceY1.isNaN()) {
            Log.e("mafei", "异常：NaN")
            return
        }

        if (this.point1.x > this.point3.x) {
            control12.set(result1)
            control23.set(result2)
        } else {
            control12.set(result2)
            control23.set(result1)
        }
    }

    private fun calculate1() {
        // 两个端点的直线斜率
        val kLine = (point5.y - point3.y) / (point5.x - point3.x)
        // 控制杆的直线 b
        val bLine = point4.y - kLine * point4.x

        // 圆方程的参数 ax2 + bx + c = 0
        val aCircle = 1 + (kLine * kLine)
        val bCircle = 2 * kLine * (bLine - point4.y) - (2 * point4.x)
        val cCircle = (point4.x * point4.x) + ((bLine - point4.y) * (bLine - point4.y)) - (distance2 * distance2)

        val x1 = (-bCircle + sqrt(bCircle * bCircle - (4 * aCircle * cCircle))) / (2 * aCircle)
        val x2 = (-bCircle - sqrt(bCircle * bCircle - (4 * aCircle * cCircle))) / (2 * aCircle)

        val y1 = kLine * x1 + bLine
        val y2 = kLine * x2 + bLine

        val result1 = PointF(x1, y1)
        val result2 = PointF(x2, y2)

        val distanceY2 = result1.y - result2.y

        Log.e("mafei", "distanceY2=" + distanceY2)
        Log.e("mafei", "distance2=" + distance2)

        if (abs(distanceY2) > distance2 * 2) {
            Log.e("mafei", "异常：太长")
            return
        }

        if (abs(distanceY2) < 1) {
            Log.e("mafei", "异常：< 1")
            return
        }

        if (distanceY2.isNaN()) {
            Log.e("mafei", "异常：NaN")
            return
        }


        if (this.point3.x > this.point5.x) {
            control34.set(result1)
            control45.set(result2)
        } else {
            control34.set(result2)
            control45.set(result1)
        }

        invalidate()
    }


    private var downXOnImg = 0f
    private var downYOnImg = 0f

    private var downType = DownType.DOWN_TYPE_NONE

    enum class DownType {
        DOWN_TYPE_P_1,
        DOWN_TYPE_P_2,
        DOWN_TYPE_P_3,
        DOWN_TYPE_P_4,
        DOWN_TYPE_P_5,
        DOWN_TYPE_NONE,
    }

    private var downX = 0f
    private var downY = 0f
    private var moveX = 0f
    private var moveY = 0f

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                downXOnImg = getXOnImg(event.x)
                downYOnImg = getYOnImg(event.y)
                downX = event.x
                downY = event.y
                if (abs(getXOnView(downXOnImg) - getXOnView(point1.x)) < 88f && abs(getYOnView(downYOnImg) - getYOnView(point1.y)) < 88f) {
                    downType = DownType.DOWN_TYPE_P_1
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point2.x)) < 88f && abs(getYOnView(downYOnImg) - getYOnView(point2.y)) < 88f) {
                    downType = DownType.DOWN_TYPE_P_2
                    isBeeline = false
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point3.x)) < 88f && abs(getYOnView(downYOnImg) - getYOnView(point3.y)) < 88f) {
                    downType = DownType.DOWN_TYPE_P_3
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point4.x)) < 88f && abs(getYOnView(downYOnImg) - getYOnView(point4.y)) < 88f) {
                    downType = DownType.DOWN_TYPE_P_4
                    isBeeline = false
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point5.x)) < 88f && abs(getYOnView(downYOnImg) - getYOnView(point5.y)) < 88f) {
                    downType = DownType.DOWN_TYPE_P_5
                } else {
                    downType = DownType.DOWN_TYPE_NONE
                    return false
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when (downType) {
                    DownType.DOWN_TYPE_P_1 -> {
                        val distanceX = event.x - downX
                        val distanceY = event.y - downY

                        downX = event.x
                        downY = event.y

                        point1.set(point1.x + distanceX / imgScale, point1.y + distanceY / imgScale)

                        if (isBeeline) {
                            // 计算 point2 的坐标
                            point2.x = (point1.x + point3.x) / 2
                            point2.y = (point1.y + point3.y) / 2
                        }

                        calculate()
                        calculate1()
                        invalidate()
                    }
                    DownType.DOWN_TYPE_P_2 -> {
                        val distanceX = event.x - downX
                        val distanceY = event.y - downY

                        downX = event.x
                        downY = event.y

                        point2.set(point2.x + distanceX / imgScale, point2.y + distanceY / imgScale)

                        calculate()
                        calculate1()
                        invalidate()
                    }
                    DownType.DOWN_TYPE_P_3 -> {
                        val distanceX = event.x - downX
                        val distanceY = event.y - downY

                        downX = event.x
                        downY = event.y

                        point3.set(point3.x + distanceX / imgScale, point3.y + distanceY / imgScale)

                        if (isBeeline) {
                            // 计算 point2 的坐标
                            point2.x = (point1.x + point3.x) / 2
                            point2.y = (point1.y + point3.y) / 2

                            // 计算 point4 的坐标
                            point4.x = (point3.x + point5.x) / 2
                            point4.y = (point3.y + point5.y) / 2
                        }

                        calculate()
                        calculate1()
                        invalidate()
                    }
                    DownType.DOWN_TYPE_P_4 -> {
                        val distanceX = event.x - downX
                        val distanceY = event.y - downY

                        downX = event.x
                        downY = event.y

                        point4.set(point4.x + distanceX / imgScale, point4.y + distanceY / imgScale)

                        calculate()
                        calculate1()
                        invalidate()
                    }
                    DownType.DOWN_TYPE_P_5 -> {
                        val distanceX = event.x - downX
                        val distanceY = event.y - downY

                        downX = event.x
                        downY = event.y

                        point5.set(point5.x + distanceX / imgScale, point5.y + distanceY / imgScale)

                        if (isBeeline) {
                            // 计算 point4 的坐标
                            point4.x = (point3.x + point5.x) / 2
                            point4.y = (point3.y + point5.y) / 2
                        }

                        calculate()
                        calculate1()
                        invalidate()
                    }
                    DownType.DOWN_TYPE_NONE -> {

                    }
                }
            }
            MotionEvent.ACTION_UP -> {
                downType = DownType.DOWN_TYPE_NONE
            }
        }
        return super.onTouchEvent(event)
    }
}