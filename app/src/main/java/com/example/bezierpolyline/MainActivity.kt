package com.example.bezierpolyline

import android.graphics.Matrix
import android.graphics.RectF
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.otaliastudios.zoom.ZoomEngine
import com.otaliastudios.zoom.ZoomLayout

class MainActivity : AppCompatActivity() {

    val bezierPolylineView2 by lazy { findViewById<BezierPolylineView2>(R.id.bezierPolylineView2) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val zoomLayout = findViewById<ZoomLayout>(R.id.zoomLayout)
        val bezierPolylineView = findViewById<BezierPolylineView>(R.id.bezierPolylineView)
        val bezierPolylineView1 = findViewById<BezierPolylineView1>(R.id.bezierPolylineView1)
        val bezierPolylineView2 = findViewById<BezierPolylineView2>(R.id.bezierPolylineView2)

        val radioGroup = findViewById<RadioGroup>(R.id.radioGroup)
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            bezierPolylineView.isQuad = checkedId == R.id.rbQuad
        }

        val rgPlan = findViewById<RadioGroup>(R.id.rgPlan)
        rgPlan.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbPlanA -> {
                    radioGroup.isInvisible = false
                    bezierPolylineView.isVisible = true
                    bezierPolylineView1.isVisible = false
                    bezierPolylineView2.isVisible = false
                }
                R.id.rbPlanB -> {
                    radioGroup.isInvisible = true
                    bezierPolylineView.isVisible = false
                    bezierPolylineView1.isVisible = true
                    bezierPolylineView2.isVisible = false
                }
                R.id.rbPlanC -> {
                    radioGroup.isInvisible = true
                    bezierPolylineView.isVisible = false
                    bezierPolylineView1.isVisible = false
                    bezierPolylineView2.isVisible = true
                }
            }
        }

        rgPlan.check(R.id.rbPlanA)
        radioGroup.check(R.id.rbCubic)

        zoomLayout.engine.addListener(object : ZoomEngine.Listener {
            override fun onIdle(engine: ZoomEngine) {

            }

            override fun onUpdate(engine: ZoomEngine, matrix: Matrix) {
                val scale = matrix.getScale()
                val rectF = RectF(0f, 0f, 1383f, 778f)
                matrix.mapRect(rectF)

                bezierPolylineView.setImageRect(rectF, scale)
                bezierPolylineView1.setImageRect(rectF, scale)
                bezierPolylineView2.setImageRect(rectF, scale)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.option, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {
            R.id.helpLine -> {
                menuItem.isChecked = !menuItem.isChecked
                bezierPolylineView2.isShowHelpLine = menuItem.isChecked
            }
        }
        return super.onOptionsItemSelected(menuItem)
    }
}