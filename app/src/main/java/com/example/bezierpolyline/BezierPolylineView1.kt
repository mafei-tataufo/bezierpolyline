package com.example.bezierpolyline

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class BezierPolylineView1 @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val path = Path()

    private val paint = Paint().apply {
        color = Color.parseColor("#FFFF00")
        style = Paint.Style.STROKE
        strokeWidth = 10f
    }

    private var pathEffect: PathEffect = DashPathEffect(floatArrayOf(15f, 10f), 0f)

    /**
     * 相对于图片的坐标
     */
    private var control1 = PointF(546f, 73f)
    private var control2 = PointF(1000f, 1400f)

    private val point1 = PointF(828f, 158f)
    private val point2 = PointF(532f, 497f)

    /**
     * 连接点
     */
    private val point3 = PointF(828f, 328f)

    private var imgRect = RectF()
    private var imgScale = 1f

    fun setImageRect(rect: RectF, scale: Float) {
        imgRect = rect
        imgScale = scale
        invalidate()
    }

    private fun getXOnView(xOnImg: Float): Float {
        return imgRect.left + xOnImg * imgScale
    }

    private fun getYOnView(yOnImg: Float): Float {
        return imgRect.top + yOnImg * imgScale
    }

    private fun getXOnImg(xOnView: Float): Float {
        return (xOnView - imgRect.left) / imgScale
    }

    private fun getYOnImg(yOnView: Float): Float {
        return (yOnView - imgRect.top) / imgScale
    }

    override fun onDraw(canvas: Canvas) {

        paint.pathEffect = pathEffect
        canvas.drawLine(getXOnView(control1.x), getYOnView(control1.y), getXOnView(point3.x), getYOnView(point3.y), paint)
        canvas.drawLine(getXOnView(point3.x), getYOnView(point3.y), getXOnView(control2.x), getYOnView(control2.y), paint)
        paint.pathEffect = null

        path.reset()
        path.moveTo(getXOnView(point1.x), getYOnView(point1.y))
        path.quadTo(getXOnView(control1.x), getYOnView(control1.y), getXOnView(point3.x), getYOnView(point3.y))

        canvas.drawPath(path, paint)

        path.reset()
        path.moveTo(getXOnView(point3.x), getYOnView(point3.y))
        path.quadTo(getXOnView(control2.x), getYOnView(control2.y), getXOnView(point2.x), getYOnView(point2.y))

        canvas.drawPath(path, paint)


        canvas.drawCircle(getXOnView(point3.x), getYOnView(point3.y), 20f, paint)

        canvas.drawCircle(getXOnView(control1.x), getYOnView(control1.y), 20f, paint)
        canvas.drawCircle(getXOnView(control2.x), getYOnView(control2.y), 20f, paint)
    }

    private var downXOnImg = 0f
    private var downYOnImg = 0f

    private var downType = -1

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        // 控制点2 的位置
        val control2X = if (control1.x - point3.x < 0) {
            // 在左边
            point3.x + abs(point3.x - control1.x)
        } else {
            // 在右边
            point3.x - abs(point3.x - control1.x)
        }
        val control2Y = if (control1.y - point3.y < 0) {
            // 在上面
            point3.y + abs(point3.y - control1.y)
        } else {
            // 在下面
            point3.y - abs(point3.y - control1.y)
        }
        control2.x = control2X
        control2.y = control2Y
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                downXOnImg = getXOnImg(event.x)
                downYOnImg = getYOnImg(event.y)
                if (abs(getXOnView(downXOnImg) - getXOnView(control1.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(control1.y)) < 50f) {
                    downType = 0
                } else if (abs(getXOnView(downXOnImg) - getXOnView(control2.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(control2.y)) < 50f) {
                    downType = 1
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point1.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(point1.y)) < 50f) {
                    downType = 2
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point2.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(point2.y)) < 50f) {
                    downType = 3
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point3.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(point3.y)) < 50f) {
                    downType = 4
                } else {
                    downType = -1
                    return false
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if (downType == 0) {
                    control1.x = getXOnImg(event.x)
                    control1.y = getYOnImg(event.y)

                    // 控制点2 的位置
                    val control2X = if (control1.x - point3.x < 0) {
                        // 在左边
                        point3.x + abs(point3.x - control1.x)
                    } else {
                        // 在右边
                        point3.x - abs(point3.x - control1.x)
                    }
                    val control2Y = if (control1.y - point3.y < 0) {
                        // 在上面
                        point3.y + abs(point3.y - control1.y)
                    } else {
                        // 在下面
                        point3.y - abs(point3.y - control1.y)
                    }
                    control2.x = control2X
                    control2.y = control2Y

                    invalidate()
                } else if (downType == 1) {
                    control2.x = getXOnImg(event.x)
                    control2.y = getYOnImg(event.y)

                    // 控制点1 的位置
                    val control2X = if (control2.x - point3.x < 0) {
                        // 在左边
                        point3.x + abs(point3.x - control2.x)
                    } else {
                        // 在右边
                        point3.x - abs(point3.x - control2.x)
                    }
                    val control2Y = if (control2.y - point3.y < 0) {
                        // 在上面
                        point3.y + abs(point3.y - control2.y)
                    } else {
                        // 在下面
                        point3.y - abs(point3.y - control2.y)
                    }
                    control1.x = control2X
                    control1.y = control2Y

                    invalidate()
                } else if (downType == 2) {
                    point1.set(getXOnImg(event.x), getYOnImg(event.y))
                    invalidate()
                } else if (downType == 3) {
                    point2.set(getXOnImg(event.x), getYOnImg(event.y))
                    invalidate()
                } else if (downType == 4) {
                    point3.set(getXOnImg(event.x), getYOnImg(event.y))
                    val dx = downXOnImg - getXOnImg(event.x)
                    val dy = downYOnImg - getYOnImg(event.y)
                    control1.set(control1.x - dx, control1.y - dy)
                    control2.set(control2.x - dx, control2.y - dy)
                    downXOnImg = getXOnImg(event.x)
                    downYOnImg = getYOnImg(event.y)

                    // 控制点1 的位置
                    val control2X = if (control2.x - point3.x < 0) {
                        // 在左边
                        point3.x + abs(point3.x - control2.x)
                    } else {
                        // 在右边
                        point3.x - abs(point3.x - control2.x)
                    }
                    val control2Y = if (control2.y - point3.y < 0) {
                        // 在上面
                        point3.y + abs(point3.y - control2.y)
                    } else {
                        // 在下面
                        point3.y - abs(point3.y - control2.y)
                    }
                    control1.x = control2X
                    control1.y = control2Y

                    invalidate()
                }
            }
            MotionEvent.ACTION_UP -> {
                downType = -1
            }
        }
        return super.onTouchEvent(event)
    }
}