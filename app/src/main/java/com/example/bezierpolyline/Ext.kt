package com.example.bezierpolyline

import android.graphics.Matrix
import android.graphics.PointF
import kotlin.math.abs
import kotlin.math.sqrt

fun Matrix.getScale(): Float {
    val values = FloatArray(9)
    this.getValues(values)
    return values[Matrix.MSCALE_X]
}

fun PointF.distance(other: PointF): Double {
    val distanceX = abs(other.x - x)
    val distanceY = abs(other.y - y)
    return sqrt(distanceX * distanceX + distanceY * distanceY.toDouble())
}
