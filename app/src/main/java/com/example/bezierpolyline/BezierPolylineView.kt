package com.example.bezierpolyline

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import kotlin.math.abs

class BezierPolylineView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val path = Path()

    var isQuad = false
        set(value) {
            field = value
            invalidate()
        }

    private val paint = Paint().apply {
        color = Color.parseColor("#FFFF00")
        style = Paint.Style.STROKE
        strokeWidth = 10f
    }

    private var pathEffect: PathEffect = DashPathEffect(floatArrayOf(15f, 10f), 0f)

    /**
     * 相对于图片的坐标
     */
    private var control1 = PointF(546f, 73f)
    private var control2 = PointF(1000f, 1400f)

    private val point1 = PointF(828f, 158f)
    private val point2 = PointF(532f, 497f)

    private var imgRect = RectF()
    private var imgScale = 1f

    fun setImageRect(rect: RectF, scale: Float) {
        imgRect = rect
        imgScale = scale
        invalidate()
    }

    private fun getXOnView(xOnImg: Float): Float {
        return imgRect.left + xOnImg * imgScale
    }

    private fun getYOnView(yOnImg: Float): Float {
        return imgRect.top + yOnImg * imgScale
    }

    private fun getXOnImg(xOnView: Float): Float {
        return (xOnView - imgRect.left) / imgScale
    }

    private fun getYOnImg(yOnView: Float): Float {
        return (yOnView - imgRect.top) / imgScale
    }

    override fun onDraw(canvas: Canvas) {

        paint.pathEffect = pathEffect
        if (isQuad) {
            canvas.drawLine(getXOnView(control1.x), getYOnView(control1.y), getXOnView(point1.x), getYOnView(point1.y), paint)
            canvas.drawLine(getXOnView(point2.x), getYOnView(point2.y), getXOnView(control1.x), getYOnView(control1.y), paint)
        } else {
            canvas.drawLine(getXOnView(point1.x), getYOnView(point1.y), getXOnView(control1.x), getYOnView(control1.y), paint)
            canvas.drawLine(getXOnView(control1.x), getYOnView(control1.y), getXOnView(control2.x), getYOnView(control2.y), paint)
            canvas.drawLine(getXOnView(control2.x), getYOnView(control2.y), getXOnView(point2.x), getYOnView(point2.y), paint)
        }
        paint.pathEffect = null

        path.reset()
        path.moveTo(getXOnView(point1.x), getYOnView(point1.y))
        if (isQuad) {
            path.quadTo(getXOnView(control1.x), getYOnView(control1.y), getXOnView(point2.x), getYOnView(point2.y))
        } else {
            path.cubicTo(getXOnView(control1.x), getYOnView(control1.y), getXOnView(control2.x), getYOnView(control2.y), getXOnView(point2.x), getYOnView(point2.y))
        }
        canvas.drawPath(path, paint)

        canvas.drawCircle(getXOnView(control1.x), getYOnView(control1.y), 20f, paint)
        if (!isQuad) {
            canvas.drawCircle(getXOnView(control2.x), getYOnView(control2.y), 20f, paint)
        }
    }

    private var downType = -1

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                val downXOnImg = getXOnImg(event.x)
                val downYOnImg = getYOnImg(event.y)
                if (abs(getXOnView(downXOnImg) - getXOnView(control1.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(control1.y)) < 50f) {
                    downType = 0
                } else if (abs(getXOnView(downXOnImg) - getXOnView(control2.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(control2.y)) < 50f) {
                    downType = 1
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point1.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(point1.y)) < 50f) {
                    downType = 2
                } else if (abs(getXOnView(downXOnImg) - getXOnView(point2.x)) < 50f && abs(getYOnView(downYOnImg) - getYOnView(point2.y)) < 50f) {
                    downType = 3
                } else {
                    downType = -1
                    return false
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if (downType == 0) {
                    control1.x = getXOnImg(event.x)
                    control1.y = getYOnImg(event.y)
                    invalidate()
                } else if (downType == 1) {
                    control2.x = getXOnImg(event.x)
                    control2.y = getYOnImg(event.y)
                    invalidate()
                } else if (downType == 2) {
                    point1.set(getXOnImg(event.x), getYOnImg(event.y))
                    invalidate()
                } else if (downType == 3) {
                    point2.set(getXOnImg(event.x), getYOnImg(event.y))
                    invalidate()
                }
            }
            MotionEvent.ACTION_UP -> {
                downType = -1
            }
        }
        return super.onTouchEvent(event)
    }
}